var express = require('express');
var router = express.Router();

// Controllers
const auth = require('../controllers/authController');

/* register page. */
router.post('/register', auth.register);

module.exports = router;
